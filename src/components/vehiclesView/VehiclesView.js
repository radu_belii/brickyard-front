import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Vehicle from '../vehicle/Vehicle';
import agent from '../../agent';
import { connect } from 'react-redux';


import {
  LOAD_VEHICLES,
  DELETE_VEHICLE,
  LOGOUT,
  EDIT_VEHICLE,
} from '../../constants/actionTypes';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between'
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

const mapStateToProps = state => ({ ...state.vehicles, ...state.common });

const mapDispatchToProps = dispatch => ({
  onFetchAllVehicles: () =>
    dispatch({ type: LOAD_VEHICLES, payload: agent.Vehicle.getAll() }),
  onDeleteVehicle: (id) =>
    dispatch({ type: DELETE_VEHICLE, payload: agent.Vehicle.delete(id), vehicleId: id }),
  onEdit: (vehicle) =>
    dispatch({type: EDIT_VEHICLE, payload: vehicle}),
  onLogOut: () => dispatch({ type: LOGOUT })
});

class VehiclesViews extends Component {
  constructor() {
    super();
    this.fetchAllVehicles = () => this.props.onFetchAllVehicles();
    this.logOut = () => this.props.onLogOut();
  }

  componentWillMount() {
    this.props.onFetchAllVehicles();
  }

  render() {
    const { classes, vehicles, errors } = this.props;

    let vehiclesContainer;

    if (vehicles) {
      vehiclesContainer = vehicles.map(vehicle =>
        <Vehicle
          delete={this.props.onDeleteVehicle}
          edit={this.props.onEdit}
          vehicleDetails={vehicle}
          key={vehicle.id}>
        </Vehicle>);
    } else {
      vehiclesContainer = <div style={{ color: 'red' }}>{errors}</div>;
    }

    if (errors) {
      this.props.onLogOut();
    }

    return (
      <>
        <Button onClick={this.fetchAllVehicles}>
          Update VEHICLES data
        </Button>
        <div className={classes.root}>
          {vehiclesContainer}
        </div>
      </>);
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(VehiclesViews));
