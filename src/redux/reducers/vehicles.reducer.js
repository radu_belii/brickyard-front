import {
  LOAD_VEHICLES,
  ASYNC_START,
  UPDATE_FIELD_SEARCH,
  SEARCH_PAGE_UNLOADED,
  SEARCH_BY_LICENSE_PLATE,
  CREATE_VEHICLE,
  LOGOUT,
  DELETE_VEHICLE,
  SEARCH_VEHICLE_IMAGE,
  EDIT_VEHICLE,
  SAVE_EDITED_VEHICLE,
  UNLOAD_EDIT_VEHICLE,
} from '../../constants/actionTypes';

const defaultState = {
  vehicleOpenData: null
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case LOAD_VEHICLES:
      return {
        ...state,
        inProgress: false,
        vehicles: action.payload.data,
        errors: action.error ? action.payload.message : null,
      }
    case UPDATE_FIELD_SEARCH:
      return { ...state, [action.key]: action.value };
    case SEARCH_PAGE_UNLOADED:
    case UNLOAD_EDIT_VEHICLE:
      return {};
    case SEARCH_BY_LICENSE_PLATE:
      return {
        ...state,
        inSearchProgress: false,
        vehicleOpenData: action.payload.data[0],
        errors: action.error ? action.payload.message : null,
      }
    case CREATE_VEHICLE:
      return {
        ...state,
        inSaveProgress: false,
        createdVehicle: action.payload.data,
        createErrors: action.error ? action.payload.message : null,
        createVehicleSucces: false,
      }
    case DELETE_VEHICLE:
      const vehicleId = action.vehicleId;
      return {
        ...state,
        vehicles: state.vehicles.filter(vehicle => vehicle.id !== vehicleId),
        inProgress: false
      }
    case EDIT_VEHICLE:
      return {
        ...state,
        editVehicle: action.payload
      }
    case SAVE_EDITED_VEHICLE:
      return {
        ...state,
        inEditeSaveProgress: false,
        editedVehicle: action.payload,
        editErrors: action.error ? action.payload.message : null,
        saveEditeVehicleSucces: false,
      }
    case SEARCH_VEHICLE_IMAGE:
      return {
        ...state,
        vehicleUrlImage: action.payload.data,
        inProgress: false,
      }
    case ASYNC_START:
      if (action.subtype === LOAD_VEHICLES || action.subtype === SEARCH_PAGE_UNLOADED) {
        return { ...state, inProgress: true };
      }
      if (action.subtype === CREATE_VEHICLE) {
        return { ...state, inSaveProgress: true, createVehicleSucces: !action.error }
      }

      if (action.subtype === SAVE_EDITED_VEHICLE) {
        return { ...state, inEditeSaveProgress: true, saveEditeVehicleSucces: !action.error }
      }

      if (action.subtype === SEARCH_BY_LICENSE_PLATE) {
        return { ...state, inSearchProgress: true }
      }

      if (action.subtype === DELETE_VEHICLE) {
        return { ...state, inProgress: true }
      }

      if (action.subtype === SEARCH_VEHICLE_IMAGE) {
        return { ...state, inProgress: true }
      }

      break;
    case LOGOUT:
      return {};
    default:
      return state;
  }
  return state;
};
