import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
  wrapper: {
    display: 'flex',
    
    justifyContent: 'center',
    opacity: 1,
    willChange: 'opacity',
    transition: 'opacity 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
    backgroundColor: 'rgba(232, 231, 231, 0.5)',
  },
  progress: {
    margin: theme.spacing.unit * 2,
    top: '50%',
    position: 'absolute'
  },
});

function Loading (props) {
  const { classes } = props;
  return (
    <div className={classes.wrapper}>
      <CircularProgress className={classes.progress} />
    </div>
  );
}

export default withStyles(styles)(Loading);