import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  wrapper: {
    margin: theme.spacing.unit,
    position: 'relative',
  },
  buttonSuccess: {
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
  buttonFail: {
    backgroundColor: red[500],
    '&:hover': {
      backgroundColor: red[700],
    },
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    top: -6,
    left: -6,
    zIndex: 1,
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
});

class SaveButton extends Component {

  handleButtonClick = () => {
    this.props.createVehicleProps.onClick();
  };
  
  render() {
    
    const { inSaveProgress, createErrors, onSuccesName } = this.props.createVehicleProps;
    const createdSucces = createErrors === null;

    const { classes } = this.props;
    const buttonClassname = classNames({
      [classes.buttonSuccess]: createdSucces,
      [classes.buttonFail]: createErrors,
    });

    return (
      <div className={classes.root}>
        <div className={classes.wrapper}>
          <Button
            variant="contained"
            color="primary"
            className={buttonClassname}
            disabled={inSaveProgress}
            onClick={this.handleButtonClick}
          >
          {`${createErrors ? 'Faild to save': (createdSucces ? onSuccesName : 'Save Vehicle Info')  }`}
          </Button>
          {inSaveProgress && <CircularProgress size={24} className={classes.buttonProgress} />}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(SaveButton);
