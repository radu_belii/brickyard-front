import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Search from '@material-ui/icons/Search';
import AddCircle from '@material-ui/icons/AddCircle';
import ViewModule from '@material-ui/icons/ViewModule';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import HeaderBar from '../headerBar/HeaderBar';

import './MainLayout.style.scss';

const mapStateToProps = state => ({ ...state.common });

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
    marginBottom: '50px',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: 0,
    [theme.breakpoints.up('sm')]: {
      width: 0
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
});

class MainLayout extends React.Component {
  state = {
    open: false,
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };
  // TODO handleDrawerClose from click on whole layout
  render() {
    const { classes, theme, isLogged } = this.props;

    const leftMenuUI = (
      <Drawer
        variant="permanent"
        className={classNames(classes.drawer, {
          [classes.drawerOpen]: this.state.open,
          [classes.drawerClose]: !this.state.open,
        })}
        classes={{
          paper: classNames({
            [classes.drawerOpen]: this.state.open,
            [classes.drawerClose]: !this.state.open,
          }),
        }}
        open={this.state.open}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={this.handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />

        <List className='left-menu__links' onClick={this.handleDrawerClose}>
          <Link to="/create">
            <ListItem button>
              <ListItemIcon>{<AddCircle />}</ListItemIcon>
              <ListItemText primary={'Add Vehicle'} />
            </ListItem>
          </Link>

          <Link to="/">
            <ListItem button>
              <ListItemIcon>{<ViewModule />}</ListItemIcon>
              <ListItemText primary={'View all vehicles'} />
            </ListItem>
          </Link>

          <Link to="/search">
            <ListItem button>
              <ListItemIcon>{<Search />}</ListItemIcon>
              <ListItemText primary={'Search'} />
            </ListItem>
          </Link>
        </List>
      </Drawer>
    )

    return (
      <div className={classes.root}>
        <CssBaseline />
        <HeaderBar stateOpen={this.state.open} handleDrawerOpen={this.handleDrawerOpen} ></HeaderBar>
        {isLogged && leftMenuUI}
      </div>
    );
  }
}


export default connect(mapStateToProps, null)(withStyles(styles, { withTheme: true })(MainLayout));
