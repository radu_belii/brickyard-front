import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import { Link } from 'react-router-dom';

const styles = {
  root: {
    width: '100%',
    maxWidth: '360px',
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 14,
  },
  cardActions: {
    justifyContent: 'space-between'
  },
  bold: {
    fontWeight: 600
  },
  card: {
    maxWidth: 360,
    width: '100%',
    marginBottom: '20px',
  },
  cardWrapper: {
    marginTop: '20px',
  },
  media: {
    height: 140,
  },
};

class Vehicle extends React.Component {
  
  render() {
    const { classes, vehicleDetails } = this.props;
    return (
      <Card className={classes.card}>
        <CardMedia
          className={classes.media}
          image={vehicleDetails.image_url || 'No image'}
          title={vehicleDetails.merk}
        />
        <CardContent>
          <List component="nav" className={classes.root}>
            <ListItem>
              <Typography gutterBottom variant="h5" component="h6">
                License plate: <span className={classes.bold}>{vehicleDetails.license_plate || 'No License plate'}</span>
              </Typography>
            </ListItem>
            <Divider />
            <ListItem button divider>
              <Typography variant="subheading" component="p">
                merk* (brand): <span className={classes.bold}> {vehicleDetails.merk || 'No vehicle data'}</span>
              </Typography>
            </ListItem>
            <ListItem button divider>
              <Typography component="p">
                handelsbenaming* (commercial name): <span className={classes.bold}> {vehicleDetails.handelsbenaming || 'No commercial name data'} </span>
              </Typography>
            </ListItem>
            <ListItem button divider>
              <Typography component="p">
                voertuigsoort (type): <span className={classes.bold}> {vehicleDetails.voertuigsoort || 'No voertuigsoort data'} </span>
              </Typography>
            </ListItem>
            <ListItem button divider>
              <Typography component="p">
                eerste_kleur* (color): <span className={classes.bold}> {vehicleDetails.eerste_kleur || 'No eerste_kleur data'} </span>
              </Typography>
            </ListItem>
            <ListItem button divider>
              <Typography component="p">
                aantal_zitplaatsen (number of seats): <span className={classes.bold}> {vehicleDetails.aantal_zitplaatsen || 'No vehicle data'} </span>
              </Typography>
            </ListItem>

            <ListItem button divider>
              <Typography component="p">
                catalogusprijs (catalog price): <span className={classes.bold}> {vehicleDetails.catalogusprijs || 'No vehicle data'} </span>
              </Typography>
            </ListItem>
          </List>
        </CardContent>
        <CardActions className={classes.cardActions}>
          <IconButton className={classes.button} aria-label="Delete">
            <DeleteIcon onClick={() => this.props.delete(vehicleDetails.id)} />
          </IconButton>

          <Link
            to="/edit">
            <Button onClick={() => this.props.edit(vehicleDetails)} >
              <EditIcon />
            </Button>
          </Link>
        </CardActions>
      </Card>
    );
  }
}


export default withStyles(styles)(Vehicle);
