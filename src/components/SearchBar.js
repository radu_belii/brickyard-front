import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ENTER_KEY } from '../constants/actionTypes';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '80%',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  wrapper: {
    display: 'flex',
  },
  submit: {
    maxHeight: '50px',
    alignSelf: 'center',
  },
});

// TODO handle onEnter click search
class Search extends Component {
  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    this.changeSearch = ev => this.props.onChangeSearch(ev.target.value);

    this.handleSeach = (searchValue) => ev => {
      ev.preventDefault();
      if (searchValue && searchValue.length > 0) {
        this.props.onSubmit(searchValue);
      }
    }

    this.onEnterClick = (searchValue) => (ev) => {
      if (ev.charCode === ENTER_KEY) {
        ev.preventDefault();
        if (searchValue && searchValue.length > 0) {
          this.props.onSubmit(searchValue);
        }
      }
    }

    const { classes, searchValue } = this.props;

    return (
      <>
        <div className={classes.wrapper} >
          <form className={classes.container}
            noValidate
            autoComplete="off"
            onKeyPress={this.onEnterClick(searchValue)}
          >
            <TextField
              onChange={this.changeSearch}
              id="search"
              label="Search car info by licenseplate"
              fullWidth
              defaultValue={searchValue}
              placeholder="Licenseplate"
              className={classes.textField}
              margin="normal"
              variant="outlined"
            />
          </form>
          <Button
            onClick={this.handleSeach(searchValue)}
            type="submit"
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            search
        </Button>
        </div>
      </>
    );
  }
}

export default withStyles(styles)(Search);
