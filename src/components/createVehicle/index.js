import React, { Component } from 'react';
import Search from '../SearchBar';
import CreateVehicleView from '../CreateVehicleView';
import Loading from './../Loading';

import { connect } from 'react-redux';
import agent from '../../agent';

import {
  CREATE_VEHICLE,
  UPDATE_FIELD_SEARCH,
  SEARCH_BY_LICENSE_PLATE,
  SEARCH_PAGE_UNLOADED,
  SEARCH_VEHICLE_IMAGE,
} from './../../constants/actionTypes';

const mapStateToProps = state => ({ ...state.vehicles });

const mapDispatchToProps = dispatch => ({
  onChangeSearch: value =>
    dispatch({ type: UPDATE_FIELD_SEARCH, key: 'searchValue', value }),
  onSubmit: (searchValue) =>
    dispatch({ type: SEARCH_BY_LICENSE_PLATE, payload: agent.Vehicle.searchByLicensePlate(searchValue) }),
  onUnload: () =>
    dispatch({ type: SEARCH_PAGE_UNLOADED }),
  onSaveClick: (vehicleInfo) =>
    dispatch({ type: CREATE_VEHICLE, payload: agent.Vehicle.createVehicle(vehicleInfo) }),
  onSearchVehicleImage: (searchKeywords) =>
    dispatch({ type: SEARCH_VEHICLE_IMAGE, payload: agent.Vehicle.getVehicleImage(searchKeywords) })
});

// TODO make more redeble UI errors
class CreateVehicle extends Component {

  constructor() {
    super();
    this.state = {
      vehicleData: {},
      searchKeywords: '',
      createErrors: null
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.vehicleOpenData) {
      const searchKeywords = `${props.vehicleOpenData.merk}+${props.vehicleOpenData.handelsbenaming}`

      if (state.searchKeywords !== searchKeywords) {
        return {
          vehicleData: props.vehicleOpenData,
          searchKeywords,
        }
      }
    }

    if (props.vehicleUrlImage) {
      let imageUrl;

      try {
        imageUrl = props.vehicleUrlImage.items[0].pagemap.cse_thumbnail[0].src;
      } catch (error) {
        console.log('[CREATE VEHICLE] NO image found');
      }
      return { imageUrl }
    }

    return null;
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.searchKeywords !== prevState.searchKeywords) {
      this.props.onSearchVehicleImage(this.state.searchKeywords);
    }
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  unLoadStateSaveButton() {

    const unloadStateButton = () => {
      console.log('unLoadStateSaveButton')
    }

    setTimeout(() => unloadStateButton(), 3000);
  }

  render() {

    let vehicleDataToSave;

    if (this.props.vehicleOpenData) {
      const vehicleData = this.props.vehicleOpenData;

      vehicleDataToSave = {
        vehicle: {
          merk: vehicleData.merk,
          handelsbenaming: vehicleData.handelsbenaming,
          eerste_kleur: vehicleData.eerste_kleur,
          license_plate: vehicleData.kenteken,
          aantal_zitplaatsen: vehicleData.aantal_zitplaatsen,
          catalogusprijs: vehicleData.catalogusprijs,
          voertuigsoort: vehicleData.voertuigsoort,
          image_url: this.state.imageUrl,
        }
      }
    }

    const onCreateVehicle = {
      onClick: () => {
        this.props.onSaveClick(vehicleDataToSave);
        this.unLoadStateSaveButton();
      },
      inSaveProgress: this.props.inSaveProgress,
      createErrors: this.props.createErrors,
      onSuccesName: 'Save Vehicle Succes',
    };

    return (<>
      <Search {...this.props} />
      <p>Example licenseplates to use: RF661H, NL706S, WNVP76</p>
      {this.props.inSearchProgress && <Loading></Loading>}
      <CreateVehicleView createVehicleProps={onCreateVehicle} imageUrl={this.state.imageUrl} {...this.props} />
      {this.props.createErrors}
    </>);
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateVehicle);