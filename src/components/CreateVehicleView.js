import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import SaveButton from './SaveButton';

const styles = {
  root: {
    width: '100%',
    maxWidth: '460px',
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 14,
  },
  bold: {
    fontWeight: 600
  },
  card: {
    maxWidth: 460,

  },
  cardWrapper: {
    marginTop: '20px',
  },
  media: {
    height: 140,
  },
};

// TODO remove span style, create more generic class
class CreateVehicleView extends Component {

  render() {
    const { classes, vehicleOpenData, imageUrl } = this.props;
    const generateVehicleDataUI = () => (
      <Card className={classes.card}>
        <CardMedia
          className={classes.media}
          image={imageUrl|| 'no image'}
          title={vehicleOpenData.merk}
        />
        <CardContent>
          <List component="nav" className={classes.root}>
            <ListItem>
              <Typography gutterBottom variant="h5" component="h4">
                License plate: <span className={classes.bold}>{vehicleOpenData.kenteken || 'No License plate'}</span>
              </Typography>
            </ListItem>
            <Divider />
            <ListItem button divider>
              <Typography component="p">
                merk* (brand): <span className={classes.bold}> {vehicleOpenData.merk || 'No vehicle data'}</span>
              </Typography>
            </ListItem>
            <ListItem button divider>
              <Typography component="p">
                handelsbenaming* (commercial name): <span className={classes.bold}> {vehicleOpenData.handelsbenaming || 'No commercial name data'} </span>
              </Typography>
            </ListItem>
            <ListItem button divider>
              <Typography component="p">
                voertuigsoort (type): <span className={classes.bold}> {vehicleOpenData.voertuigsoort || 'No voertuigsoort data'} </span>
              </Typography>
            </ListItem>
            <ListItem button divider>
              <Typography component="p">
                eerste_kleur* (color): <span className={classes.bold}> {vehicleOpenData.eerste_kleur || 'No eerste_kleur data'} </span>
              </Typography>
            </ListItem>
            <ListItem button divider>
              <Typography component="p">
                aantal_zitplaatsen (number of seats): <span className={classes.bold}> {vehicleOpenData.aantal_zitplaatsen || 'No vehicle data'} </span>
              </Typography>
            </ListItem>
  
            <ListItem button divider>
              <Typography component="p">
                catalogusprijs (catalog price): <span className={classes.bold}> {vehicleOpenData.catalogusprijs || 'No vehicle data'} </span>
              </Typography>
            </ListItem>
          </List>
        </CardContent>
        <CardActions>
          <SaveButton createVehicleProps={this.props.createVehicleProps} />
        </CardActions>
      </Card>
    )
    // TODO rename variables  
    // Review Logic
    let openDataVehicleInfoUi = null;
  
    if (vehicleOpenData) {
      openDataVehicleInfoUi = generateVehicleDataUI();
    } else if (Array.isArray(vehicleOpenData) && vehicleOpenData.length === 0) {
      // response from server empty object
      openDataVehicleInfoUi = 'Something was wrong, try again'
    }
  
    return (
      <div className={classes.cardWrapper}>
        {openDataVehicleInfoUi}
      </div>
    );
  }
  }
  
export default withStyles(styles)(CreateVehicleView);