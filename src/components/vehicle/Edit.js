import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import SaveButton from '../SaveButton';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { SAVE_EDITED_VEHICLE, UNLOAD_EDIT_VEHICLE } from './../../constants/actionTypes';
import agent from '../../agent';

import './Edit.style.scss'
const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: '360px',
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 14,
  },

  cardActions: {
    justifyContent: 'space-between'
  },
  bold: {
    fontWeight: 600
  },
  card: {
    maxWidth: 360,
    marginBottom: '20px',
  },
  cardWrapper: {
    marginTop: '20px',
  },
  media: {
    height: 140,
  },
});

const mapStateToProps = state => ({ ...state.vehicles });

const mapDispatchToProps = dispatch => ({
  onSave: (id, vehicle) => {
    dispatch({ type: SAVE_EDITED_VEHICLE, payload: agent.Vehicle.edit(id, vehicle) })
  },
  onUnload: () => {
    dispatch({ type: UNLOAD_EDIT_VEHICLE})
  }
})

class Edit extends React.Component {

  constructor() {
    super();

    this.state = {
      updateVehicleData: { vehicle: {}}
    }

    this.onChange = (key) => ev => {
    
      const updateVehicle = { ...this.state.updateVehicleData};

      if (updateVehicle.vehicle.key) {
        updateVehicle.vehicle.key = ev.target.value
      } else {
        updateVehicle.vehicle[key] = ev.target.value
      }

      this.setState({
        updateVehicleData: updateVehicle
      })
    }
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const { classes, editVehicle, inEditeSaveProgress } = this.props;

    console.log(editVehicle)

    const onEditVehicle = {
      onClick: () => this.props.onSave(this.props.editVehicle.id, this.state.updateVehicleData),
      inSaveProgress: inEditeSaveProgress,
      createErrors: this.props.editErrors,
      onSuccesName: 'Edit Vehicle Succes',
      buttonName: 'Save'
    };
    return (
      <Card className={classes.card}>
        <CardMedia
          className={classes.media}
          image={editVehicle.image_url || 'No img'}
          title={editVehicle.merk}
        />
        <CardContent className='edit-container'>
        <TextField
            onChange={this.onChange('image_url')}
            label="Image URL:"
            fullWidth
            defaultValue={editVehicle.image_url || 'No (image_url) data'}
            placeholder="image_url"
            margin="normal"
            variant="outlined"
          />
          <TextField
            onChange={this.onChange('license_plate')}
            label="License plate:"
            fullWidth
            defaultValue={editVehicle.license_plate || 'No (license plate) data'}
            placeholder="Licenseplate"
            margin="normal"
            variant="outlined"
          />
          <TextField
            onChange={this.onChange('merk')}
            label="merk* (brand):"
            fullWidth
            defaultValue={editVehicle.merk || 'No (brand) data'}
            placeholder="merk* (brand)"
            margin="normal"
            variant="outlined"
          />

          <TextField
            onChange={this.onChange('handelsbenaming')}
            label="handelsbenaming* (commercial name):"
            fullWidth
            defaultValue={editVehicle.handelsbenaming || 'No (commercial name) data'}
            placeholder="handelsbenaming* (commercial name)"
            margin="normal"
            variant="outlined"
          />

          <TextField
            onChange={this.onChange('voertuigsoort')}
            label="voertuigsoort (type):"
            fullWidth
            defaultValue={editVehicle.voertuigsoort || 'No (type) data'}
            placeholder="voertuigsoort (type)"
            margin="normal"
            variant="outlined"
          />

          <TextField
            onChange={this.onChange('eerste_kleur')}
            label="eerste_kleur* (color):"
            fullWidth
            defaultValue={editVehicle.eerste_kleur || 'No (color) data'}
            placeholder="eerste_kleur* (color)"
            margin="normal"
            variant="outlined"
          />

          <TextField
            onChange={this.onChange('aantal_zitplaatsen')}
            label="aantal_zitplaatsen (number of seats):"
            fullWidth
            defaultValue={editVehicle.aantal_zitplaatsen || 'No (number of seats) data'}
            placeholder="aantal_zitplaatsen (number of seats)"
            margin="normal"
            variant="outlined"
          />

          <TextField
            onChange={this.onChange('catalogusprijs')}
            label="catalogusprijs (catalog price):"
            fullWidth
            defaultValue={editVehicle.catalogusprijs || 'No (catalog price) data'}
            placeholder="catalogusprijs (catalog price)"
            margin="normal"
            variant="outlined"
          />

        </CardContent>
        <CardActions className={classes.cardActions}>
          <SaveButton createVehicleProps={onEditVehicle} />
        </CardActions>
      </Card>
    );
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Edit));
