import React, { Component } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Button from '@material-ui/core/Button';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { LOGOUT } from '../../constants/actionTypes';

const mapStateToProps = state => ({ ...state.common });

const mapDispatchToProps = dispatch => ({
  onLogOut: () => dispatch({ type: LOGOUT })
});

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  flexGrow: {
    flexGrow: 1
  },
  aTag: {
    color: '#ffffff',
    textDecoration: 'none'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
});

class HeaderBar extends Component {
  state = {
    auth: true,
    anchorEl: null,
  };

  handleChange = event => {
    this.setState({ auth: event.target.checked });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleLogOut = () => {
    this.handleClose();
    this.props.onLogOut();

  }

  render() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

    const { classes, stateOpen, handleDrawerOpen, isLogged } = this.props;

    const LogInRegisterUI = (
      <>
        <Link to="/login" className={classes.aTag}>
          <Button color="inherit">Login</Button>
        </Link>

        <Link to="/register" className={classes.aTag}>
          <Button color="inherit">Register</Button>
        </Link>
      </>
    );

    const openButtonUI = (
      <IconButton
        color="inherit"
        aria-label="Open drawer"
        onClick={handleDrawerOpen}
        className={classNames(classes.menuButton, {
          [classes.hide]: stateOpen,
        })}
      >
        <MenuIcon />
      </IconButton>)

    const UserMenuUI = (<>
      <div>
        <IconButton
          aria-owns={open ? 'menu-appbar' : undefined}
          aria-haspopup="true"
          onClick={this.handleMenu}
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={open}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.handleClose}>Profile</MenuItem>
          <MenuItem onClick={this.handleClose}>My account</MenuItem>
          <MenuItem onClick={this.handleLogOut}>Log Out</MenuItem>
        </Menu>
      </div>

    </>);


    return (
      <AppBar
        position="fixed"
        className={classNames(classes.appBar, {
          [classes.appBarShift]: stateOpen,
        })}
      >
        <Toolbar disableGutters={!stateOpen}>
          {isLogged ? openButtonUI : null}
          <Typography style={isLogged ? null : { marginLeft: '15px' }} className={classes.flexGrow} variant="h6" color="inherit" noWrap>
            <Link className={classes.aTag}
              to="/">
              Brickyard
            </Link>
          </Typography>
          {isLogged ? UserMenuUI : LogInRegisterUI}
        </Toolbar>
      </AppBar>
    )
  }

}


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(HeaderBar));