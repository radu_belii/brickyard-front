import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { LOAD_VEHICLES } from '../../constants/actionTypes';
import Vehicle from './../vehicles/Vehicle';
import agent from '../../agent';
import { connect } from 'react-redux';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

const mapStateToProps = state => ({ ...state.vehicles, ...state.common });

const mapDispatchToProps = dispatch => ({
  onFetchAllVehicles: () =>
    dispatch({ type: LOAD_VEHICLES, payload: agent.Vehicle.getAll() }),
});

class VehiclesViews extends Component {
  constructor() {
    super();
    this.fetchAllVehicles = () => this.props.onFetchAllVehicles();
  }

  componentWillMount() {
    this.props.onFetchAllVehicles();
  }

  render() {
    const { classes, vehicles, errors } = this.props;

    let vehiclesContainer;

    if (vehicles) {
      vehiclesContainer = vehicles.map(vehicle => <Vehicle vehicleDetails={vehicle} key={vehicle.id}> </Vehicle>);
    } else {
      vehiclesContainer = <div style={{ color: 'red' }}>{errors}</div>
    }

    return (
      <>
        <Button onClick={this.fetchAllVehicles}
        >
          FETCH VEHICLES
      </Button>

        <div className={classes.root}>
          {vehiclesContainer}
        </div>
      </>);
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(VehiclesViews));
