export const ENTER_KEY = 13;

export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const REGISTER = 'REGISTER';
export const SUCCES_REGISTER = 'SUCCES_REGISTER';
export const APP_LOAD = 'APP_LOAD';
export const REDIRECT = 'REDIRECT';
export const UPDATE_FIELD_AUTH = 'UPDATE_FIELD_AUTH';
export const LOGIN_PAGE_UNLOADED = 'LOGIN_PAGE_UNLOADED';
export const REGISTER_PAGE_UNLOADED = 'REGISTER_PAGE_UNLOADED';
export const ASYNC_START = 'ASYNC_START';
export const ASYNC_END = 'ASYNC_END';
export const LOGIN_STATUS = 'LOGIN_STATUS';

// --------------- vehicle------------------
export const LOAD_VEHICLES = 'LOAD_VEHICLES';
export const UPDATE_FIELD_SEARCH  = 'UPDATE_FIELD_SEARCH';
export const SEARCH_PAGE_UNLOADED  = 'SEARCH_PAGE_UNLOADED';
export const SEARCH_BY_LICENSE_PLATE  = 'SEARCH_BY_LICENSE_PLATE';
export const CREATE_VEHICLE = 'CREATE_VEHICLE';
export const DELETE_VEHICLE = 'DELETE_VEHICLE';
export const SEARCH_VEHICLE_IMAGE = 'SEARCH_VEHICLE_IMAGE';
export const EDIT_VEHICLE = 'EDIT_VEHICLE';
export const SAVE_EDITED_VEHICLE = 'SAVE_EDITED_VEHICLE';
export const UNLOAD_EDIT_VEHICLE = 'UNLOAD_EDIT_VEHICLE';
