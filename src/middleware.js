import agent from './agent';

import {
  ASYNC_START,
  ASYNC_END,
  LOGIN,
  LOGOUT,
  REGISTER,
  SUCCES_REGISTER
} from './constants/actionTypes';

const promiseMiddleware = store => next => action => {
  
  if (isPromise(action.payload)) {
    store.dispatch({ type: ASYNC_START, subtype: action.type });
    const currentView = store.getState().viewChangeCounter;
    const skipTracking = action.skipTracking;

    action.payload.then(
      res => {
        const currentState = store.getState()
        if (!skipTracking && currentState.viewChangeCounter !== currentView) {
          return
        }

        action.payload = res;
        store.dispatch({ type: ASYNC_END, promise: action.payload });
        store.dispatch(action);
      },

      error => {
        const currentState = store.getState()
        if (!skipTracking && currentState.viewChangeCounter !== currentView) {
          return
        }
        action.error = true;
        action.payload = error.response.data;
        if (!action.skipTracking) {
          store.dispatch({ type: ASYNC_END, promise: action.payload });
        }
        store.dispatch(action);
      }
    );

    return;
  }

  next(action);
};

const localStorageMiddleware = store => next => action => {
  
  if (action.type === LOGIN) {
    if (!action.error) {
      window.localStorage.setItem('auth_token', action.payload.data.auth_token);

      // setting token to local scope (instance) 
      agent.setToken(action.payload.data.auth_token);
    }
  } else if (action.type === LOGOUT) {
    window.localStorage.setItem('auth_token', '');
    agent.setToken(null);
  }

  next(action);
};

const userRegisterMiddleware = store => next => action => {
  if (action.type === REGISTER) {
    store.dispatch({ type: SUCCES_REGISTER, payload: action.payload.data });
  }

  next(action);
}

function isPromise(v) {
  return v && typeof v.then === 'function';
}

export { promiseMiddleware, localStorageMiddleware, userRegisterMiddleware }
