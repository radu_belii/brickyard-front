import {
  APP_LOAD,
  REDIRECT,
  LOGOUT,
  LOGIN_PAGE_UNLOADED,
  LOGIN,
  REGISTER,
  LOGIN_STATUS,
} from '../../constants/actionTypes';

const defaultState = {
  appName: 'Brickyard',
  token: null,
  viewChangeCounter: 0,
  isLogged: false,
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case APP_LOAD:
      return {
        ...state,
        token: action.token || null,
        appLoaded: true,
      };
    case REDIRECT:
      return { ...state, redirectTo: null };
    case LOGIN:
    case REGISTER:
      return {
        ...state,
        token: action.error ? null : action.payload.data.auth_token,
        errors: action.error ? action.payload.message : null,
        isLogged: action.error ? false : true,
        redirectTo: action.error ? null : '/',
        currentUser: action.error ? null : action.payload.user
      }
    case LOGOUT:
      return { token: null, isLogged: false, redirectTo: '/' };
    case LOGIN_STATUS:
      return { ...state, isLogged: action.payload };
    case LOGIN_PAGE_UNLOADED:
      return { ...state, viewChangeCounter: state.viewChangeCounter + 1 };
    default:
      return state;
  }
};
