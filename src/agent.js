import axios from 'axios';

const API_ROOT = 'https://arcane-scrubland-64110.herokuapp.com/';
const API_OPEN_DATA = 'https://opendata.rdw.nl/resource/m9d7-ebf2.json?kenteken=';

const API_GOOGLE_ROOT = 'https://www.googleapis.com/customsearch/v1?q=';
const API_GOOGLE_KEY = 'AIzaSyCYE0gXeY5tIWlJYucl0ZAuBwQ7gdyuIQM';
const CX_GOOGLE = '009973936156204013139:nrb9qxuk0na';


let token = null;

const config = () => {
  return {
    headers: {
      'Authorization': "Bearer " + token,
      "Content-Type": "application/json",
    }
  }
}

const requests = {
  del: url => axios.delete(`${API_ROOT}${url}`, config()),
  get: (url) => axios.get(`${API_ROOT}${url}`, config()),
  put: (url, body) => axios.put(`${API_ROOT}${url}`, body, config()),
  post: (url, body) => axios.post(`${API_ROOT}${url}`, body, config())
};

const Auth = {
  login: (email, password) =>
    requests.post('auth/login', { email, password }),
  register: (username, email, password) => requests.post('users/', { username, email, password }),
};

const Vehicle = {
  getAll: () => requests.get('vehicles/'),
  searchByLicensePlate: (plateNumber) => axios.get(`${API_OPEN_DATA}${plateNumber}`),
  createVehicle: (body) => requests.post('vehicles/', body),
  delete: (id) => requests.del('vehicles/' + id),
  edit: (id, body) =>  requests.put('vehicles/' + id, body),
  getVehicleImage: (query) => axios.get(`${API_GOOGLE_ROOT}${query}&cx=${CX_GOOGLE}&num=1&key=${API_GOOGLE_KEY}`),
}

export default {
  Auth,
  Vehicle,
  setToken: _token => { token = _token }
};