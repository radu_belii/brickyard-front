import React, { Component } from 'react';
import Search from '../SearchBar';
import Loading from './../Loading';
import { connect } from 'react-redux';
import agent from '../../agent';

import {
  CREATE_VEHICLE,
  UPDATE_FIELD_SEARCH,
  SEARCH_BY_LICENSE_PLATE,
  SEARCH_PAGE_UNLOADED,
  SEARCH_VEHICLE_IMAGE,
} from './../../constants/actionTypes';

const mapStateToProps = state => ({ ...state.vehicles });
const mapDispatchToProps = dispatch => ({
  onChangeSearch: value =>
    dispatch({ type: UPDATE_FIELD_SEARCH, key: 'searchValue', value }),
  onSubmit: (searchValue) =>
    dispatch({ type: SEARCH_BY_LICENSE_PLATE, payload: agent.Vehicle.searchByLicensePlate(searchValue) }),
  onUnload: () =>
    dispatch({ type: SEARCH_PAGE_UNLOADED }),
  onSaveClick: (vehicleInfo) =>
    dispatch({ type: CREATE_VEHICLE, payload: agent.Vehicle.createVehicle(vehicleInfo) }),
  onSearchVehicleImage: (searchKeywords) =>
    dispatch({ type: SEARCH_VEHICLE_IMAGE, payload: agent.Vehicle.getVehicleImage(searchKeywords) })
});


class SearchVehicle extends Component {

  render() {

    return (
      <>
        <div>SearchVehicle</div>
        <p> ...In develop mode</p>
        <Search {...this.props}/>
      </>
    )
  }
}

export default  connect(mapStateToProps, mapDispatchToProps) (SearchVehicle);
