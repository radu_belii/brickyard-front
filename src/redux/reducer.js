import auth from './reducers/auth.reducer';
import common from './reducers/common.reducer';
import vehicles from './reducers/vehicles.reducer';

import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';


export default combineReducers({
  auth,
  common,
  vehicles,
  router: routerReducer
});
