import React, { Component } from 'react';
import SignIn from './login/Login.js';
import Home from './home/index.js';
import Register from './Register';
import MainLayout from './../components/mainLayout/MainLayout'
import Loading from './Loading';
import CreateVehicle from './createVehicle/index';
import Edit from '../components/vehicle/Edit';
import agent from '../agent';
import SearchVehicle from '../components/SearchVehicle/index'

import { Route, Switch } from 'react-router-dom';
import { store } from '../redux/store';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import { REDIRECT, LOAD_VEHICLES, LOGIN_STATUS } from '../constants/actionTypes';

import './App.scss';


const mapStateToProps = state => {
  return {
    inProgress: state.vehicles.inProgress,
    appLoaded: state.common.appLoaded,
    redirectTo: state.common.redirectTo
  }
};

const mapDispatchToProps = dispatch => ({
  onFetchAllVehicles: () =>
    dispatch({ type: LOAD_VEHICLES, payload: agent.Vehicle.getAll() }),
  onRedirect: () =>
    dispatch({ type: REDIRECT }),
  onSetLoggedIn: () => 
    dispatch({ type: LOGIN_STATUS, payload: true }),
});

class App extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.redirectTo) {
      store.dispatch(push(nextProps.redirectTo));
      this.props.onRedirect();
    }
  }

  componentWillMount() {
    const token = window.localStorage.getItem('auth_token');
    if (token) {
      agent.setToken(token);

      this.props.onSetLoggedIn();
    }
  }

  render() {
    const { inProgress } = this.props;
    
    return (
      <div className="App">
        <MainLayout />
        {inProgress && <Loading ></Loading>}
        <main className='container'>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/login" component={SignIn} />
            <Route path="/register" component={Register} />
            <Route path="/create" component={CreateVehicle}/>
            <Route path="/edit" component={Edit}/>
            <Route path="/search" component={SearchVehicle}/>
          </Switch>
        </main>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
